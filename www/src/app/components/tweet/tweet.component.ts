

import { Component, Input, Output, EventEmitter } from '@angular/core';
import { TweetsService } from '../../tweets/services/tweets.service';
import { Router } from '@angular/router';

@Component({
    selector: 'tweet-component',
    templateUrl: './tweet.component.html',
    styleUrls: ['./tweet.component.less'],
})
export class TweetComponent {

    public colors: Array<string> = [
        "#FC7641",
        "#F22A1F",
        "#991651",
        "#F7942A",
        "#177E89",
        "#1B4079",
        "#FFC857",
        "#323031",
        "#000080",
        "#1E90FF",
        "#550F52"
    ]

    public pickedColor: string;

    @Input('tweet') public tweet;
    @Output() savedStatusChanged: EventEmitter<any> = new EventEmitter();

    public get isSaved() {
        return this.tweetsService.isSaved(this.tweet.id_str);
    }

    constructor(
        public tweetsService: TweetsService,
        private router: Router
    ){}

    ngOnInit() {
        this.getRandomColor();
    }

    getRandomColor() {
        let range = this.colors.length;
        let number = Math.floor(Math.random() * range);
        this.pickedColor =  this.colors[number];
    }

    getSavedColor(tweet) {
        return this.tweetsService.isSaved(tweet);
    }

    toggleSave(event: Event) {
        event.stopPropagation();
        if(this.tweetsService.isSaved(this.tweet.id_str)) {
            this.tweetsService.removeSavedTweet(this.tweet.id_str)
        } else {
            this.tweetsService.addSavedTweet(this.tweet);
        }
        this.savedStatusChanged.emit('Status Changed');
    }

    goToDetails() {
        this.router.navigate(['details', this.tweet.id_str]);
    }
}