
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { TweetsService } from './services/tweets.service';

@Component({
    selector: 'tweets-component',
    templateUrl: './tweets.component.html',
    styleUrls: ['./tweets.component.less'],
    providers: [TweetsService]
})
export class TweetsComponent {

    private nameToSearch: string;
    public tweets: any;

    constructor(
        private route: ActivatedRoute,
        private tweetsService: TweetsService
    ){}

    ngOnInit() {
        this.route.params.subscribe(
            params => {
                this.nameToSearch = params.name;
                this.getTweets();
            }
        )
    }

    getTweets() {
        this.tweetsService.getTweets(this.nameToSearch)
            .subscribe(
                response => {
                    this.tweets = response.statuses;
                },
                error => {
                    console.log(error);
                }
            )
    }


  
}