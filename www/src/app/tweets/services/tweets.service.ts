
import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class TweetsService {


    constructor(
        private http: HttpClient
    ){}


    getTweets(name: string): Observable<any> {
        return this.http.get(`/api/getTweets/${name}`)
    }


    getSavedTweets() {
        let tweets = localStorage.getItem('tweets')
        if(tweets)
            return JSON.parse(tweets);
        else
            return null;
    }

    addSavedTweet(tweet) {
        let tweets = this.getSavedTweets();
        let tweetToSave = {
            id_str: tweet.id_str,
            text: tweet.text,
            favorite_count: tweet.favorite_count,
            retweet_count: tweet.retweet_count,
            user: {
                name: tweet.user.name,
                profile_image_url: tweet.user.profile_image_url
            },
            entities: tweet.entities
        }
        if(tweets) 
            tweets.push(tweetToSave);
        else 
            tweets = [tweetToSave]

        localStorage.setItem('tweets', JSON.stringify(tweets));
    }

    isSaved(tweetId: string) {
        let tweets = this.getSavedTweets();
        return (tweets && tweets.map(x => x.id_str).indexOf(tweetId) != -1)
    }

    removeSavedTweet(tweetId: string) {
        let tweets = this.getSavedTweets();
        let index =  tweets.map(x => x.id_str).indexOf(tweetId);
        tweets.splice(index, 1);
        localStorage.setItem('tweets', JSON.stringify(tweets));
    }

    

}