import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http'
import { Observable } from 'rxjs/Observable';

import { Injectable, Injector } from '@angular/core';

@Injectable()
export class InterceptedHttp implements HttpInterceptor {
    constructor(
    ) {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        // let modifiedRequest = req.clone({
        //     url: `/api/${req.url}`
        // })

        return next.handle(req);
    }
}