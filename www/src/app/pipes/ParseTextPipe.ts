

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'ParseText'})
export class ParseTextPipe implements PipeTransform {
  transform(value: string, urls: Array<any>): string {
    if(urls.length == 0)
        return value;
    let splittedString = value.split(" ");
    for(let url of urls) {
        let index = splittedString.indexOf(url.url);
        splittedString[index] = `<a href="${url.expanded_url}"> ${url.url} <a>`;
    }

    return splittedString.join(" ");
  }
}