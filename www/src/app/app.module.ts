import { BrowserModule } from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms'
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AsyncPipe  } from '@angular/common/'


import { InterceptedHttp } from './common/InterceptedHttp';
import { AppComponent } from './app.component';
import { AppRoutingModule, routedComponents } from './app.routing';

import { TweetComponent } from './components/tweet/tweet.component';
import { ParseTextPipe } from'./pipes/ParseTextPipe';


@NgModule({
  declarations: [
    AppComponent,
    routedComponents,
    TweetComponent,
    ParseTextPipe
   ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptedHttp,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }



