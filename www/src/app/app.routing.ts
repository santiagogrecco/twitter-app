import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomeComponent } from './home/home.component';
import { TweetsComponent } from './tweets/tweets.component';
import { DetailsComponent } from './details/details.component';
import { AsyncPipe } from '@angular/common';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
    },
    {
        path: "home",
        component: HomeComponent,
    },
    {
        path: "tweets/:name",
        component: TweetsComponent
    },
    {
        path: "details/:id",
        component: DetailsComponent
    }
]


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: []
  })
  export class AppRoutingModule { }



export const routedComponents = [HomeComponent, TweetsComponent, DetailsComponent];