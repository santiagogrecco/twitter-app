
import { Component } from '@angular/core';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams  } from '@angular/common/http';
import { TweetsService } from '../tweets/services/tweets.service';

@Component({
    selector: 'home-component',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.less'],
    providers: [TweetsService]
})
export class HomeComponent  {

    public searchText: string;
    public savedTweets: any;

    constructor(
        private http: HttpClient,
        private router: Router,
        private tweetsService: TweetsService
    ){}

    ngOnInit() {
        this.getSaved();
    }

    getSaved() {
        this.savedTweets = this.tweetsService.getSavedTweets();        
    }

    search() {
        this.router.navigate(['tweets', this.searchText])
    }

    

}

