
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { DetailsService } from './services/details.service';

@Component({
    selector: 'details-component',
    templateUrl: './details.component.html',
    styleUrls: ['./details.component.less'],
    providers: [DetailsService]
})
export class DetailsComponent {
    
    public tweetId: string;
    public tweet: any;

    constructor(
        private route: ActivatedRoute,
        private detailsService: DetailsService
    ){}

    ngOnInit() {
        this.route.params.subscribe(
            params => {
                this.tweetId = params.id;
                this.getDetails();
            }
        )
    }

    getDetails() {
        this.detailsService.getDetails(this.tweetId)
            .subscribe(
                response => {
                    this.tweet = response;
                },
                error => console.log(error)
            )
    }
}