import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DetailsService {

    constructor(
        private http: HttpClient 
    ){}

    getDetails(id: string) {
        return this.http.get(`/api/getTweet/${id}`)
    }
}