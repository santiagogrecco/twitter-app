

const router = require("express").Router();
const axios = require("axios");
const  tokenUrl = "https://api.twitter.com/oauth2/token";
const basicAuthToken = "Basic U3dhSjlCTk5ZdkVzYjVHc2luclEyZnBxMDo2TE5lM3R3SE9PUWhrTVU5emU0a1I3dXpJa3lNam9Kb21ZS0g4NklyQWNKeHR1OVJPTA=="

let bearerToken = "Bearer AAAAAAAAAAAAAAAAAAAAAA4G3QAAAAAAf4un2ly%2Byu2DdQrUyjPVyxW%2FgIc%3DBHZlpuUHBSXfCVojpZIbK2evjEaG6EwAg0EuabIqNW5JtqG0Th";

router.route("/getTweets/:name")
    .all((req, res, next) => {
        if(!bearerToken)
            getToken();
        next();
    })
    .get((req, res) => {
        let name = req.params.name;
        
        //#region  configs
        let baseUrl = "https://api.twitter.com/1.1/search/tweets.json";
        let configs = {
            headers: {
                "Authorization": bearerToken
            },
            params: {
                "q": name,
                "result_type": "mixed"
            }
        }
        //#endregion
        
        
        axios.get(baseUrl, configs)
        .then(response => res.json(response.data))
        .catch(error => {
            res.end();
            console.log(error)
        });
    })


router.route("/getTweet/:id")
    .get((req, res) => {
        let id = req.params.id;

        //#region configs
        let baseUrl = " https://api.twitter.com/1.1/statuses/show.json";
        let configs = {
            headers: {
                "Authorization": bearerToken
            },
            params: {
                "id": id
            }
        }
        //endregion

        axios.get(baseUrl, configs)
        .then(response => res.json(response.data))
        .catch(error => {
            res.end();
            console.log(error)
        });


    })









function getToken() {
    let body = {
        grant_type: "client_credentials"
    }
    let configs = {
        'data': {
            'grant_type': "client_credentials"
        },
        'headers': {
            'Content-Type': 'application/x-www-form-urlencoded',            
            'Authorization': 'Basic U3dhSjlCTk5ZdkVzYjVHc2luclEyZnBxMDo2TE5lM3R3SE9PUWhrTVU5emU0a1I3dXpJa3lNam9Kb21ZS0g4NklyQWNKeHR1OVJPTA=='
        }
    }
    axios.post("https://api.twitter.com/oauth2/token", configs)
        .then(response => bearerToken = `Bearer ${response.access_token}`)
        .catch(error => console.log(error.response.data));
}

module.exports = router;