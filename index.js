
const axios = require("axios");
const express = require("express");
const path = require("path");
const app = express();                 
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, "/www/dist")));


var port = process.env.PORT || 3000;

var router = require("./routes/router");


app.use("/api", router);

app.listen(port, ()=> {
    console.log(`Listening on port: ${port}`);
})
